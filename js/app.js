$(document).ready(function() {
    setContentDivSize();

    if (!isUserKnown()) {
        $('#content').load('views/welcome-page.html');
    } else {
        $('#content').load('views/menu.html', function() {
            refreshOrderList();
            setMenuViewListeners();
            setInterval(function() {
                refreshOrderList();
                fetchForDrawResults();
            }, 60000);
        });
    }
});

function setMenuViewListeners() {
    $('#placeOrderButton').click(function() {
        if (validateOrderInput()) { 
            postOrder();
        }
    });

    $('#trigger-algorithm').click(function() {
        findTheLuckyPerson();
    });
}

function validateOrderInput() {
    var dishInput = $('#dishInput');
    var placeInput = $('#placeInput');
    var priceInput = $('#priceInput');

    if (dishInput.val() === '' || placeInput.val() === '' || priceInput.val() === '') {
        $('#order-form').addClass('has-warning');
        return false;
    }

    return true;
}

function postOrder() {
    var user = Cookies.get('userName');
    var dish = $('#dishInput').val();
    var place = $('#placeInput').val();
    var price = $('#priceInput').val();

    $.post("http://127.0.0.1:8080/orders/place", 
        { 
            "user": user, 
            "dish": dish,
            "place": place,
            "price": price 
        }
    ).done(function(data) {
        if (data === "success") {
            refreshOrderList();
        }
    });
}

function refreshOrderList() {
    $.get("http://127.0.0.1:8080/orders/today", function(data) {
        var ordersList = $('#orders-list');
        ordersList.empty();

        var count = 1;
        $.each(data, function(index, value) {
            appendToOrdersList(count++, value.person, value.dish, value.place, value.price);
        });
    });
}

function appendToOrdersList(count, person, dish, place, price) {
    var ordersList = $('#orders-list');
    ordersList
        .append($('<tr>')
            .append($('<td>')
                .text(count))
            .append($('<td>')
                .text(person))
            .append($('<td>')
                .text(dish))
            .append($('<td>')
                .text(place))
            .append($('<td>')
                .text(price))
        );
}

function findTheLuckyPerson() {
    $.post("http://127.0.0.1:8080/draw/conduct", function(data) {
        fetchForDrawResults();
    });
}

function fetchForDrawResults() {
    $.get("http://127.0.0.1:8080/draw/today", function(data) {
        if (data.winner !== null) {
            $('#ordering-person-message').text(data.winner);
            $('#ordering-person-message-panel').fadeIn(400);
        }
    });
}