function fetchUserIdentity() {
    return Cookies.get('userName');
}

function isUserKnown() {
    var userName = fetchUserIdentity();
    if (userName === null || userName === undefined || userName === '') {
        return false;
    }  

    return true;
}

function storeUserIdentity(name) {
    Cookies.set('userName', name);
}