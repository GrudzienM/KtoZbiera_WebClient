function setContentDivSize() {
    $('#content').width( window.innerWidth - 500 );
    $('#content').height( window.innerHeight );

    $(window).resize(function() {
        $('#content').width( window.innerWidth - 500 );
        $('#content').height( window.innerHeight );
    });
}